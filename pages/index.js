import React from "react";
import LeftSidebar from '../components/Sidebar/LeftSidebar.js';
import RightSidebar from '../components/Sidebar/RightSidebar.js';
import HomeSection from './user/home';

export default function Home() {
  return (
    <>
      <div className="d-flex" style={{width:"100vw",height:"100vh",position:"relative"}}>
        <LeftSidebar/>
        <HomeSection />
        <RightSidebar/>
      </div>
    </>
  )
}
