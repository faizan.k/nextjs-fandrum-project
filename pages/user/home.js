import React, { Component, useEffect, useState } from 'react'
import styles from '../../styles/Home.module.css';
import Navbar from '../../components/Navbar/Navbar';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPaperPlane } from '@fortawesome/free-solid-svg-icons';
import { faEllipsisVertical } from '@fortawesome/free-solid-svg-icons';
import ReactPlayer from 'react-player';
import { Button, Col, Container, Row } from 'reactstrap';
import OfficialVideos from '../../components/Carousel/OfficialVideos';
import EntertainmentNews from '../../components/Carousel/EntertainmentNews';
import StoryCarousel from '../../components/Carousel/StoryCarousel';
import PostCarousel from '../../components/Carousel/PostCarousel';
import { getAxios } from '../../api/serverRequest';

export default function home() {

  const [googleNews,setGoogleNews] = useState([]);

  useEffect(() => {
    fetchGoogleNews();
  },[])

	const params = {
		apiKey: "3b7c92acb55e4c8d990e92e3de64b08b",
		country: "in",
		category: "entertainment"
	}

	const fetchGoogleNews = async () => {
		const response = await getAxios({ endPoint: "/top-headlines", params: params, apiType: "googleNews" })
		if (response && response.data && response.data.articles) {
			const { articles } = response.data
			setGoogleNews(articles)
      console.log(articles);
    }
	}

    return (
      <>
        <div className={styles.home_main_parent}>
          <Navbar />
          <div className={styles.story_parent_div}>
            <StoryCarousel />
          </div>
          <div className={styles.write_post_parent_div}>
            <img src="assets/images/user.png" className={styles.user_icon_class} />
            <div className={styles.write_post_input_div}>
              <input type="text" placeholder="Write something to post..." className={styles.write_post_input_class} />
              <FontAwesomeIcon icon={faPaperPlane} className={styles.search_icon_class} />
            </div>
            <img src='assets/images/post_option.png' className={styles.write_post_icon_class} />
            <img src='Images/Group 291.svg' className={styles.write_post_icon_class} />
            <img src='Images/Group 297.svg' className={styles.write_post_icon_class} />
            <img src='Images/Group 445.svg' className={styles.write_post_icon_class} />
            <img src='Images/Group 517.svg' className={styles.write_post_icon_class} />
            <img src='Images/Group 518.svg' className={styles.write_post_icon_class} />
          </div>
          <h4 className='my-4 mx-5'>Official Videos</h4>
          <div className={styles.official_video_carousel_parent}>
            <OfficialVideos />
          </div>
          <h4 className='my-4 mx-5'>Entertainment News</h4>
          <div className={styles.entertainment_news_carousel_parent}>
            <EntertainmentNews data={googleNews}/>
          </div>
          <h4 className='my-4 mx-5'>Posts</h4>
          <div className={styles.post_parent_div}>
            <div className={styles.post_video_div}>
              <ReactPlayer url='https://www.youtube.com/watch?v=ysz5S6PUM-U' className={styles.post_video_class}/>
            </div>
            <div className={styles.post_content_div}>
              <div className={styles.post_content_first_div}>
                <div className={styles.post_content_first_left}>
                  <img src='assets/images/post_user.png' className={styles.post_user_icon_class} />
                  <div className={styles.post_user_name_class}>
                    <p className='mx-3'>David</p>
                  </div>
                </div>
                <div className={styles.post_content_first_right}>
                  <Button color="secondary" className='px-4 py-2 my-3' style={{backgroundColor:"#610085",borderRadius:"20px"}}>+ Friend</Button>
                  <FontAwesomeIcon icon={faEllipsisVertical} className={styles.post_option_icon_class} />
                </div>
              </div>
              <p className='px-4'>
              #winter2021 #trending Lorem ipsum dolor sit amet, consectetur adipiscing elit. Posuere vulputate molestie ac facilisis ullamcorper risus lobortis quis vulputate. Quis diam at aliquam eget id id eget. Velit dis ut eget nec interdum id euismod massa. Nulla nulla ultricies vivamus in eu etiam eu sit. Morbi diam aliquam diam sagittis luctus egestas sit posuere. Quis ultrices habitasse enim et, lobortis et aliquam.
              </p>
              <div className={styles.user_reaction_count_div}>
                <div className={styles.user_reaction_inner}>
                  <img src='assets/images/reaction_user_icon.png' className={styles.reaction_user_icon_class}/>
                  <img src='assets/images/reaction_user_icon.png' className={styles.reaction_user_icon_class1}/>
                  <img src='assets/images/reaction_user_icon.png' className={styles.reaction_user_icon_class2}/>
                </div>
                <p style={{color:"#610085" , marginTop:"10px", marginLeft:"20px", cursor:"pointer"}}>& 7 more fandoms</p>
              </div>
              <div className={styles.reaction_parent_div}>
                <div className={styles.reaction_inner_div}>
                  <div className={styles.reaction_like_div}>
                    <img src="assets/images/like_icon.png" width="25px" style={{height:"fit-content"}}/>
                    <p className='mx-2 my-1'>531</p>
                  </div>
                  <div className={styles.reaction_like_div}>
                    <img src="assets/images/comment_icon.png" width="25px" style={{height:"fit-content", marginTop:"3px"}}/>
                    <p className='mx-2 my-1'>531</p>
                  </div>
                  <div className={styles.reaction_like_div}>
                    <img src="assets/images/share_icon.png" width="30px" style={{height:"fit-content", marginTop:"3px"}}/>
                  </div>
                  <div className={styles.reaction_like_div}>
                    <img src="assets/images/speaker_icon.png" width="30px" style={{height:"fit-content", marginTop:"3px"}}/>
                  </div>
                </div>
                <img src='assets/images/post_add_reaction_icon.png'/>
              </div>
              <div className={styles.post_comment_parent_div}>
                <img src='assets/images/user.png'/>
                <input type="text" className={styles.comment_input_div} placeholder="Write a comment...."/>
                <FontAwesomeIcon icon={faPaperPlane} className={styles.comment_icon_class} />
              </div>
            </div>
          </div>
          <div className={styles.post_parent_div}>
            <div className={styles.post_content_div}>
              <div className={styles.post_content_first_div}>
                <div className={styles.post_content_first_left}>
                  <img src='assets/images/post_user.png' className={styles.post_user_icon_class} />
                  <div className={styles.post_user_name_class}>
                    <p className='mx-3'>David</p>
                  </div>
                </div>
                <div className={styles.post_content_first_right}>
                  <Button color="secondary" className='px-4 py-2 my-3' style={{backgroundColor:"#610085",borderRadius:"20px"}}>+ Friend</Button>
                  <FontAwesomeIcon icon={faEllipsisVertical} className={styles.post_option_icon_class} />
                </div>
              </div>
              <p className='px-4'>
              #winter2021 #trending Lorem ipsum dolor sit amet, consectetur adipiscing elit. Posuere vulputate molestie ac facilisis ullamcorper risus lobortis quis vulputate. Quis diam at aliquam eget id id eget. Velit dis ut eget nec interdum id euismod massa. Nulla nulla ultricies vivamus in eu etiam eu sit. Morbi diam aliquam diam sagittis luctus egestas sit posuere. Quis ultrices habitasse enim et, lobortis et aliquam.
              </p>
              <div className={styles.user_reaction_count_div}>
                <div className={styles.user_reaction_inner}>
                  <img src='assets/images/reaction_user_icon.png' className={styles.reaction_user_icon_class}/>
                  <img src='assets/images/reaction_user_icon.png' className={styles.reaction_user_icon_class1}/>
                  <img src='assets/images/reaction_user_icon.png' className={styles.reaction_user_icon_class2}/>
                </div>
                <p style={{color:"#610085" , marginTop:"10px", marginLeft:"20px", cursor:"pointer"}}>& 7 more fandoms</p>
              </div>
              <div className={styles.reaction_parent_div}>
                <div className={styles.reaction_inner_div}>
                  <div className={styles.reaction_like_div}>
                    <img src="assets/images/like_icon.png" width="25px" style={{height:"fit-content"}}/>
                    <p className='mx-2 my-1'>531</p>
                  </div>
                  <div className={styles.reaction_like_div}>
                    <img src="assets/images/comment_icon.png" width="25px" style={{height:"fit-content", marginTop:"3px"}}/>
                    <p className='mx-2 my-1'>531</p>
                  </div>
                  <div className={styles.reaction_like_div}>
                    <img src="assets/images/share_icon.png" width="30px" style={{height:"fit-content", marginTop:"3px"}}/>
                  </div>
                  <div className={styles.reaction_like_div}>
                    <img src="assets/images/speaker_icon.png" width="30px" style={{height:"fit-content", marginTop:"3px"}}/>
                  </div>
                </div>
                <img src='assets/images/post_add_reaction_icon.png'/>
              </div>
              <div className={styles.post_comment_parent_div}>
                <img src='assets/images/user.png'/>
                <input type="text" className={styles.comment_input_div} placeholder="Write a comment...."/>
                <FontAwesomeIcon icon={faPaperPlane} className={styles.comment_icon_class} />
              </div>
            </div>
          </div>
          <div className={styles.post_parent_div}>
            <div className={styles.post_video_div}>
              <img src='assets/images/second_post_image.jpg' className={styles.post_video_class}/>
            </div>
            <div className={styles.post_content_div}>
              <div className={styles.post_content_first_div}>
                <div className={styles.post_content_first_left}>
                  <img src='assets/images/post_user.png' className={styles.post_user_icon_class} />
                  <div className={styles.post_user_name_class}>
                    <p className='mx-3'>David</p>
                  </div>
                </div>
                <div className={styles.post_content_first_right}>
                  <Button color="secondary" className='px-4 py-2 my-3' style={{backgroundColor:"#610085",borderRadius:"20px"}}>+ Friend</Button>
                  <FontAwesomeIcon icon={faEllipsisVertical} className={styles.post_option_icon_class} />
                </div>
              </div>
              <p className='px-4'>
              #winter2021 #trending Lorem ipsum dolor sit amet, consectetur adipiscing elit. Posuere vulputate molestie ac facilisis ullamcorper risus lobortis quis vulputate. Quis diam at aliquam eget id id eget. Velit dis ut eget nec interdum id euismod massa. Nulla nulla ultricies vivamus in eu etiam eu sit. Morbi diam aliquam diam sagittis luctus egestas sit posuere. Quis ultrices habitasse enim et, lobortis et aliquam.
              </p>
              <div className={styles.user_reaction_count_div}>
                <div className={styles.user_reaction_inner}>
                  <img src='assets/images/reaction_user_icon.png' className={styles.reaction_user_icon_class}/>
                  <img src='assets/images/reaction_user_icon.png' className={styles.reaction_user_icon_class1}/>
                  <img src='assets/images/reaction_user_icon.png' className={styles.reaction_user_icon_class2}/>
                </div>
                <p style={{color:"#610085" , marginTop:"10px", marginLeft:"20px", cursor:"pointer"}}>& 7 more fandoms</p>
              </div>
              <div className={styles.reaction_parent_div}>
                <div className={styles.reaction_inner_div}>
                  <div className={styles.reaction_like_div}>
                    <img src="assets/images/like_icon.png" width="25px" style={{height:"fit-content"}}/>
                    <p className='mx-2 my-1'>531</p>
                  </div>
                  <div className={styles.reaction_like_div}>
                    <img src="assets/images/comment_icon.png" width="25px" style={{height:"fit-content", marginTop:"3px"}}/>
                    <p className='mx-2 my-1'>531</p>
                  </div>
                  <div className={styles.reaction_like_div}>
                    <img src="assets/images/share_icon.png" width="30px" style={{height:"fit-content", marginTop:"3px"}}/>
                  </div>
                  <div className={styles.reaction_like_div}>
                    <img src="assets/images/speaker_icon.png" width="30px" style={{height:"fit-content", marginTop:"3px"}}/>
                  </div>
                </div>
                <img src='assets/images/post_add_reaction_icon.png'/>
              </div>
              <div className={styles.post_comment_parent_div}>
                <img src='assets/images/user.png'/>
                <input type="text" className={styles.comment_input_div} placeholder="Write a comment...."/>
                <FontAwesomeIcon icon={faPaperPlane} className={styles.comment_icon_class} />
              </div>
            </div>
          </div>
          <h3 style={{marginTop:"30px",marginLeft:"5%"}}>Explore</h3>
          <div style={{marginTop:"10px",marginLeft:"5%"}} className={styles.explore_btn_parent}>
            <Button
              color="danger"
              className={styles.explore_btn}
            >
              Movies
            </Button>
            <Button
              color="danger"
              outline
              className={styles.explore_btn}
            >
              Shows
            </Button>
            <Button
              color="danger"
              outline
              className={styles.explore_btn}
            >
              Characters
            </Button>
            <Button
              color="danger"
              outline
              className={styles.explore_btn}
            >
              Friends
            </Button>
            <Button
              color="danger"
              outline
              className={styles.explore_btn}
            >
              Celebrities
            </Button>
          </div>
          <div className={styles.post_parent_div}>
            <div className={styles.post_video_div}>
              <PostCarousel/>
            </div>
            <div className={styles.post_content_div}>
              <div className={styles.post_content_first_div}>
                <div className={styles.post_content_first_left}>
                  <img src='assets/images/post_user.png' className={styles.post_user_icon_class} />
                  <div className={styles.post_user_name_class}>
                    <p className='mx-3'>David</p>
                  </div>
                </div>
                <div className={styles.post_content_first_right}>
                  <Button color="secondary" className='px-4 py-2 my-3' style={{backgroundColor:"#610085",borderRadius:"20px"}}>+ Friend</Button>
                  <FontAwesomeIcon icon={faEllipsisVertical} className={styles.post_option_icon_class} />
                </div>
              </div>
              <p className='px-4'>
              #winter2021 #trending Lorem ipsum dolor sit amet, consectetur adipiscing elit. Posuere vulputate molestie ac facilisis ullamcorper risus lobortis quis vulputate. Quis diam at aliquam eget id id eget. Velit dis ut eget nec interdum id euismod massa. Nulla nulla ultricies vivamus in eu etiam eu sit. Morbi diam aliquam diam sagittis luctus egestas sit posuere. Quis ultrices habitasse enim et, lobortis et aliquam.
              </p>
              <div className={styles.user_reaction_count_div}>
                <div className={styles.user_reaction_inner}>
                  <img src='assets/images/reaction_user_icon.png' className={styles.reaction_user_icon_class}/>
                  <img src='assets/images/reaction_user_icon.png' className={styles.reaction_user_icon_class1}/>
                  <img src='assets/images/reaction_user_icon.png' className={styles.reaction_user_icon_class2}/>
                </div>
                <p style={{color:"#610085" , marginTop:"10px", marginLeft:"20px", cursor:"pointer"}}>& 7 more fandoms</p>
              </div>
              <div className={styles.reaction_parent_div}>
                <div className={styles.reaction_inner_div}>
                  <div className={styles.reaction_like_div}>
                    <img src="assets/images/like_icon.png" width="25px" style={{height:"fit-content"}}/>
                    <p className='mx-2 my-1'>531</p>
                  </div>
                  <div className={styles.reaction_like_div}>
                    <img src="assets/images/comment_icon.png" width="25px" style={{height:"fit-content", marginTop:"3px"}}/>
                    <p className='mx-2 my-1'>531</p>
                  </div>
                  <div className={styles.reaction_like_div}>
                    <img src="assets/images/share_icon.png" width="30px" style={{height:"fit-content", marginTop:"3px"}}/>
                  </div>
                  <div className={styles.reaction_like_div}>
                    <img src="assets/images/speaker_icon.png" width="30px" style={{height:"fit-content", marginTop:"3px"}}/>
                  </div>
                </div>
                <img src='assets/images/post_add_reaction_icon.png'/>
              </div>
              <div className={styles.post_comment_parent_div}>
                <img src='assets/images/user.png'/>
                <input type="text" className={styles.comment_input_div} placeholder="Write a comment...."/>
                <FontAwesomeIcon icon={faPaperPlane} className={styles.comment_icon_class} />
              </div>
            </div>
          </div>
          <div className={styles.post_parent_div}>
            <div className={styles.post_video_div}>
              <ReactPlayer url='https://www.youtube.com/watch?v=ysz5S6PUM-U' className={styles.post_video_class}/>
            </div>
            <div className={styles.post_content_div}>
              <div className={styles.post_content_first_div}>
                <div className={styles.post_content_first_left}>
                  <img src='assets/images/post_user.png' className={styles.post_user_icon_class} />
                  <div className={styles.post_user_name_class}>
                    <p className='mx-3'>David</p>
                  </div>
                </div>
                <div className={styles.post_content_first_right}>
                  <Button color="secondary" className='px-4 py-2 my-3' style={{backgroundColor:"#610085",borderRadius:"20px"}}>+ Friend</Button>
                  <FontAwesomeIcon icon={faEllipsisVertical} className={styles.post_option_icon_class} />
                </div>
              </div>
              <p className='px-4'>
              #winter2021 #trending Lorem ipsum dolor sit amet, consectetur adipiscing elit. Posuere vulputate molestie ac facilisis ullamcorper risus lobortis quis vulputate. Quis diam at aliquam eget id id eget. Velit dis ut eget nec interdum id euismod massa. Nulla nulla ultricies vivamus in eu etiam eu sit. Morbi diam aliquam diam sagittis luctus egestas sit posuere. Quis ultrices habitasse enim et, lobortis et aliquam.
              </p>
              <div className={styles.user_reaction_count_div}>
                <div className={styles.user_reaction_inner}>
                  <img src='assets/images/reaction_user_icon.png' className={styles.reaction_user_icon_class}/>
                  <img src='assets/images/reaction_user_icon.png' className={styles.reaction_user_icon_class1}/>
                  <img src='assets/images/reaction_user_icon.png' className={styles.reaction_user_icon_class2}/>
                </div>
                <p style={{color:"#610085" , marginTop:"10px", marginLeft:"20px", cursor:"pointer"}}>& 7 more fandoms</p>
              </div>
              <div className={styles.reaction_parent_div}>
                <div className={styles.reaction_inner_div}>
                  <div className={styles.reaction_like_div}>
                    <img src="assets/images/like_icon.png" width="25px" style={{height:"fit-content"}}/>
                    <p className='mx-2 my-1'>531</p>
                  </div>
                  <div className={styles.reaction_like_div}>
                    <img src="assets/images/comment_icon.png" width="25px" style={{height:"fit-content", marginTop:"3px"}}/>
                    <p className='mx-2 my-1'>531</p>
                  </div>
                  <div className={styles.reaction_like_div}>
                    <img src="assets/images/share_icon.png" width="30px" style={{height:"fit-content", marginTop:"3px"}}/>
                  </div>
                  <div className={styles.reaction_like_div}>
                    <img src="assets/images/speaker_icon.png" width="30px" style={{height:"fit-content", marginTop:"3px"}}/>
                  </div>
                </div>
                <img src='assets/images/post_add_reaction_icon.png'/>
              </div>
              <div className={styles.post_comment_parent_div}>
                <img src='assets/images/user.png'/>
                <input type="text" className={styles.comment_input_div} placeholder="Write a comment...."/>
                <FontAwesomeIcon icon={faPaperPlane} className={styles.comment_icon_class} />
              </div>
            </div>
          </div>  
          <h3 style={{marginTop:"30px",marginLeft:"5%"}}>Quiz</h3>
          <div className={styles.quiz_parent_div}>
            <div className={styles.quiz_img_div}>
              <img src='assets/images/quiz_img3.jpeg' width="300px" className='mx-2'/>
              <img src='assets/images/quiz_img2.jpg' width="250px" className='mx-2'/>
              <img src='assets/images/quiz_img1.jpg' width="250px" className='mx-2'/>
            </div>
            <p className={styles.quiz_question_text}>Question: <span style={{color:"#FF005C"}}>Name the other female actress from the movie Padmaavat?</span></p>
            <Container>
              <Row>
                <Col lg="6" md="6" sm="12" className={styles.quiz_options_div}>
                  <Button
                    color="danger"
                    outline
                    className={styles.quiz_options_btn}  
                  >
                    Priyanka Chopra
                  </Button>
                  <Button
                    color="danger"
                    outline
                    className={styles.quiz_options_btn}
                  >
                    Anushka Sharma
                  </Button>
                </Col>
                <Col lg="6" md="6" sm="12" className={styles.quiz_options_div}>
                  <Button
                    color="danger"
                    className={styles.quiz_options_btn}  
                  >
                    Aditi Roa Hydari
                  </Button>
                  <Button
                    color="danger"
                    outline
                    className={styles.quiz_options_btn}
                  >
                    Alia Bhatt
                  </Button>
                </Col>
              </Row>
            </Container>
          </div>
        </div>
      </>
    )
}
