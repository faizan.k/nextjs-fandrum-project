import axios from "axios";

// const rapidApiHeader = {
//     'x-rapidapi-host': process.env.REACT_APP_X_RAPID_API_HOST,
//     'x-rapidapi-key': process.env.REACT_APP_X_RAPID_API_KEY
// }

// "https://api.themoviedb.org/3/movie/{movie_id}/keywords?api_key=<<api_key>>"
export const getAxios = async ({ endPoint = "", params = {}, apiType = "rapidApi" }) => {
    const serverParams = getServerUrl(apiType)
    console.log(serverParams);
    const options = {
        method: 'GET',
        url: `${serverParams.baseApiUrl}${endPoint}`,
        params: params,
        headers: serverParams.header
    };
    try {
        const response = await axios.request(options);
        return response;
    } catch (error) {   
        return error;
    }

}
const getServerUrl = (url) => {
    switch (url) {
        case 'googleNews':
            return { baseApiUrl: `${process.env.NEXT_PUBLIC_GOOGLE_NEWS_API_URL}`, header: {} }  //changes made here
            break;
        default:
            const token = localStorage.getItem("Token")?  JSON.parse(localStorage.getItem("Token")) : ""
            return { baseApiUrl: process.env.REACT_APP_AUTH_URL, header: {"Authorization" : `Bearer ${token ? token.token : ""}`} }
            
    }
}