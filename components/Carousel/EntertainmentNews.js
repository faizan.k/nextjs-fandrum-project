import React, { useState, useEffect, useCallback } from "react";
import { PrevButton, NextButton } from "./EntertainmentNewsBtns";
import useEmblaCarousel from "embla-carousel-react";
import emblaStyles from "../../styles/embla.module.css";
import styles from '../../styles/Home.module.css';
import { Button } from "reactstrap";

const EmblaCarousel = (props) => {
  console.log("Data - " + props.data);
  const [viewportRef, embla] = useEmblaCarousel({
    containScroll: "keepSnaps",
    dragFree: true
  });
  const [prevBtnEnabled, setPrevBtnEnabled] = useState(false);
  const [nextBtnEnabled, setNextBtnEnabled] = useState(false);

  const scrollPrev = useCallback(() => embla && embla.scrollPrev(), [embla]);
  const scrollNext = useCallback(() => embla && embla.scrollNext(), [embla]);
  const onSelect = useCallback(() => {
    if (!embla) return;
    setPrevBtnEnabled(embla.canScrollPrev());
    setNextBtnEnabled(embla.canScrollNext());
  }, [embla]);

  useEffect(() => {
    if (!embla) return;
    embla.on("select", onSelect);
    onSelect();
  }, [embla, onSelect]);

  return (
    <>

    <div className={emblaStyles.embla}>
      <div className={emblaStyles.embla__viewport} ref={viewportRef}>
        <div className={emblaStyles.embla__container}>
        {
          props.data.map(item => {
            console.log(item)
            return(

            <div className={emblaStyles.embla__slide}>
            <div className={emblaStyles.embla__slide__inner}>
              <div className={styles.official_video_card_parent}>
                <div className={styles.offiicial_video_container_div}>
                  <img src={item.urlToImage} className={styles.official_video_player_class} />
                </div>
                <p className="my-2 px-3" style={{ fontSize: "18px" }}>{item.description}</p>
                <span className="my-2 px-3" style={{ fontSize: "14px" }}>4,147,102 views Dec 2, 2021</span>
                <div className={styles.official_content_first_div}>
                  <div className={styles.post_content_first_left}>
                    <img src='assets/images/post_user.png' width="40px" height="40px" />
                    <div className={styles.post_user_name_class}>
                      <p className='mx-3 my-2'>David</p>
                    </div>
                  </div>
                  <div className={styles.post_content_first_right}>
                    <Button color="secondary" className='px-4 py-1 my-1' style={{ backgroundColor: "#610085", borderRadius: "20px" }}>Follow</Button>
                  </div>
                </div>
                <div className={styles.official_user_reaction_count_div}>
                  <div className={styles.user_reaction_inner}>
                    <img src='assets/images/reaction_user_icon.png' className={styles.official_reaction_user_icon_class} />
                    <img src='assets/images/reaction_user_icon.png' className={styles.official_reaction_user_icon_class1} />
                    <img src='assets/images/reaction_user_icon.png' className={styles.official_reaction_user_icon_class2} />
                  </div>
                  <p style={{ color: "#610085", marginTop: "3px", marginLeft: "15px", cursor: "pointer" }}>& 7 more fandoms</p>
                </div>
                <div className={styles.official_reaction_parent_div}>
                  <div className={styles.reaction_inner_div}>
                    <div className={styles.official_reaction_like_div}>
                      <img src="assets/images/like_icon.png" width="25px" style={{ height: "fit-content" }} />
                      <p className='mx-2 my-1'>531</p>
                    </div>
                    <div className={styles.official_reaction_like_div}>
                      <img src="assets/images/comment_icon.png" width="25px" style={{ height: "fit-content", marginTop: "3px" }} />
                      <p className='mx-2 my-1'>531</p>
                    </div>
                    <div className={styles.official_reaction_like_div}>
                      <img src="assets/images/share_icon.png" width="25px" style={{ height: "fit-content", marginTop: "3px" }} />
                    </div>
                    <div className={styles.official_reaction_like_div}>
                      <img src="assets/images/speaker_icon.png" width="30px" style={{ height: "fit-content", marginTop: "3px",marginLeft:"10px" }} />
                    </div>
                  </div>
                  <img src='assets/images/post_add_reaction_icon.png' width="25px" height="25px"/>
                </div>
              </div>
            </div>
            </div>
            )

          })  
        }
        </div>
      </div>
      <PrevButton onClick={scrollPrev} enabled={prevBtnEnabled} />
      <NextButton onClick={scrollNext} enabled={nextBtnEnabled} />
    </div>
    </>
  );
};

export default EmblaCarousel;
