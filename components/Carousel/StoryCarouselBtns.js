import React from "react";
import styles from "../../styles/embla.module.css";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleLeft } from '@fortawesome/free-solid-svg-icons';
import { faAngleRight } from '@fortawesome/free-solid-svg-icons';
export const PrevButton = ({ enabled, onClick }) => (
  <button
    className={styles.embla__button__story + " " + styles.embla__button__prev}
    onClick={onClick}
    disabled={!enabled}
  >
    <FontAwesomeIcon icon={faAngleLeft} style={{fontSize:"15px"}}  />
  </button>
);

export const NextButton = ({ enabled, onClick }) => (
  <button
    className={styles.embla__button__story + " " + styles.embla__button__next}
    onClick={onClick}
    disabled={!enabled}
  >
      <FontAwesomeIcon icon={faAngleRight} style={{fontSize:"15px"}} />
  </button>
);
