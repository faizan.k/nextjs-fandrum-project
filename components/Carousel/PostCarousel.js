import React, { useState, useEffect, useCallback } from "react";
import { PrevButton, NextButton } from "./PostCarouselBtns";
import useEmblaCarousel from "embla-carousel-react";
import emblaStyles from "../../styles/embla.module.css";
import styles from '../../styles/Home.module.css';

const EmblaCarousel = () => {
  const [viewportRef, embla] = useEmblaCarousel({
    containScroll: "keepSnaps",
    dragFree: true
  });
  const [prevBtnEnabled, setPrevBtnEnabled] = useState(false);
  const [nextBtnEnabled, setNextBtnEnabled] = useState(false);

  const scrollPrev = useCallback(() => embla && embla.scrollPrev(), [embla]);
  const scrollNext = useCallback(() => embla && embla.scrollNext(), [embla]);
  const onSelect = useCallback(() => {
    if (!embla) return;
    setPrevBtnEnabled(embla.canScrollPrev());
    setNextBtnEnabled(embla.canScrollNext());
  }, [embla]);

  useEffect(() => {
    if (!embla) return;
    embla.on("select", onSelect);
    onSelect();
  }, [embla, onSelect]);

  return (
    <div className={emblaStyles.embla}>
      <div className={emblaStyles.embla__viewport} ref={viewportRef}>
        <div className={emblaStyles.embla__container}>
          <div className={emblaStyles.embla__slide}>
            <div className={emblaStyles.embla__slide__inner}>
              <div className={styles.official_video_card_parent}>
                <div className={styles.offiicial_video_container_div}>
                  <img src="assets/images/carousel_post_img1.jpg" className={styles.official_video_player_class} height="500px"/>
                </div>
                <p className="my-2 px-3" style={{ fontSize: "18px" }}>Palm Springs</p>
                <span className="my-2 px-3" style={{ fontSize: "14px" }}>2020</span>
                <br />
                <span className="my-2 px-3" style={{ fontSize: "14px" , color:"#FF005C"}}>40 Fandoms</span>
              </div>
            </div>
          </div>
          <div className={emblaStyles.embla__slide}>
            <div className={emblaStyles.embla__slide__inner}>
              <div className={styles.official_video_card_parent}>
                <div className={styles.offiicial_video_container_div}>
                  <img src="assets/images/carousel_post_img2.jpg" className={styles.official_video_player_class} height="500px"/>
                </div>
                <p className="my-2 px-3" style={{ fontSize: "18px" }}>Palm Springs</p>
                <span className="my-2 px-3" style={{ fontSize: "14px" }}>2020</span>
                <br />
                <span className="my-2 px-3" style={{ fontSize: "14px" , color:"#FF005C"}}>40 Fandoms</span>
              </div>
            </div>
          </div>
          <div className={emblaStyles.embla__slide}>
            <div className={emblaStyles.embla__slide__inner}>
              <div className={styles.official_video_card_parent}>
                <div className={styles.offiicial_video_container_div}>
                  <img src="assets/images/carousel_post_img3.jpeg" className={styles.official_video_player_class} height="500px"/>
                </div>
                <p className="my-2 px-3" style={{ fontSize: "18px" }}>Palm Springs</p>
                <span className="my-2 px-3" style={{ fontSize: "14px" }}>2020</span>
                <br />
                <span className="my-2 px-3" style={{ fontSize: "14px" , color:"#FF005C"}}>40 Fandoms</span>
              </div>
            </div>
          </div>
          <div className={emblaStyles.embla__slide}>
            <div className={emblaStyles.embla__slide__inner}>
              <div className={styles.official_video_card_parent}>
                <div className={styles.offiicial_video_container_div}>
                  <img src="assets/images/carousel_post_img4.jpg" className={styles.official_video_player_class} height="500px"/>
                </div>
                <p className="my-2 px-3" style={{ fontSize: "18px" }}>Palm Springs</p>
                <span className="my-2 px-3" style={{ fontSize: "14px" }}>2020</span>
                <br />
                <span className="my-2 px-3" style={{ fontSize: "14px" , color:"#FF005C"}}>40 Fandoms</span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <PrevButton onClick={scrollPrev} enabled={prevBtnEnabled} />
      <NextButton onClick={scrollNext} enabled={nextBtnEnabled} />
    </div>
  );
};

export default EmblaCarousel;
