import React, { useState, useEffect, useCallback } from "react";
import { PrevButton, NextButton } from "./StoryCarouselBtns";
import useEmblaCarousel from "embla-carousel-react";
import emblaStyles from "../../styles/embla.module.css";
import styles from '../../styles/Home.module.css';
import { Button } from "reactstrap";

const EmblaCarousel = () => {
  const [viewportRef, embla] = useEmblaCarousel({
    containScroll: "keepSnaps",
    dragFree: true
  });
  const [prevBtnEnabled, setPrevBtnEnabled] = useState(false);
  const [nextBtnEnabled, setNextBtnEnabled] = useState(false);

  const scrollPrev = useCallback(() => embla && embla.scrollPrev(), [embla]);
  const scrollNext = useCallback(() => embla && embla.scrollNext(), [embla]);
  const onSelect = useCallback(() => {
    if (!embla) return;
    setPrevBtnEnabled(embla.canScrollPrev());
    setNextBtnEnabled(embla.canScrollNext());
  }, [embla]);

  useEffect(() => {
    if (!embla) return;
    embla.on("select", onSelect);
    onSelect();
  }, [embla, onSelect]);

  return (
    <div className={emblaStyles.embla}>
      <div className={emblaStyles.embla__viewport} ref={viewportRef}>
        <div className={emblaStyles.embla__container}>
          <div className={emblaStyles.embla__slide}>
            <div className={emblaStyles.embla__slide__inner}>
                <div className={styles.story_box_div}>
                    <img src='assets/images/story_user.png' />
                    <p className={styles.story_text_class}>Quinn</p>
                </div>
            </div>
          </div>
          <div className={emblaStyles.embla__slide}>
            <div className={emblaStyles.embla__slide__inner}>
                <div className={styles.story_box_div}>
                    <img src='Images/Ellipse 91.png' />
                    <p className={styles.story_text_class}>Quinn</p>
                </div>
            </div>
          </div>
          <div className={emblaStyles.embla__slide}>
            <div className={emblaStyles.embla__slide__inner}>
                <div className={styles.story_box_div}>
                    <img src='Images/Ellipse 89.png' />
                    <p className={styles.story_text_class}>Quinn</p>
                </div>
            </div>
          </div>
          <div className={emblaStyles.embla__slide}>
            <div className={emblaStyles.embla__slide__inner}>
                <div className={styles.story_box_div}>
                    <img src='Images/Ellipse 90.png' />
                    <p className={styles.story_text_class}>Quinn</p>
                </div>
            </div>
          </div>
          <div className={emblaStyles.embla__slide}>
            <div className={emblaStyles.embla__slide__inner}>
                <div className={styles.story_box_div}>
                    <img src='Images/Ellipse 92.png' />
                    <p className={styles.story_text_class}>Quinn</p>
                </div>
            </div>
          </div>
          <div className={emblaStyles.embla__slide}>
            <div className={emblaStyles.embla__slide__inner}>
                <div className={styles.story_box_div}>
                    <img src='Images/Ellipse 93.png' />
                    <p className={styles.story_text_class}>Quinn</p>
                </div>
            </div>
          </div>
          <div className={emblaStyles.embla__slide}>
            <div className={emblaStyles.embla__slide__inner}>
                <div className={styles.story_box_div}>
                    <img src='Images/Ellipse 94.png' />
                    <p className={styles.story_text_class}>Quinn</p>
                </div>
            </div>
          </div>
          <div className={emblaStyles.embla__slide}>
            <div className={emblaStyles.embla__slide__inner}>
                <div className={styles.story_box_div}>
                    <img src='Images/Ellipse 95.png' />
                    <p className={styles.story_text_class}>Quinn</p>
                </div>
            </div>
          </div>
          <div className={emblaStyles.embla__slide}>
            <div className={emblaStyles.embla__slide__inner}>
                <div className={styles.story_box_div}>
                    <img src='Images/Ellipse 96.png' />
                    <p className={styles.story_text_class}>Quinn</p>
                </div>
            </div>
          </div>
          <div className={emblaStyles.embla__slide}>
            <div className={emblaStyles.embla__slide__inner}>
                <div className={styles.story_box_div}>
                    <img src='Images/Ellipse 97.png' />
                    <p className={styles.story_text_class}>Quinn</p>
                </div>
            </div>
          </div>
          
        </div>
      </div>
      <PrevButton onClick={scrollPrev} enabled={prevBtnEnabled} />
      <NextButton onClick={scrollNext} enabled={nextBtnEnabled} />
    </div>
  );
};

export default EmblaCarousel;
