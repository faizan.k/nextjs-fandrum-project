import React, { useEffect } from 'react'
import { Button } from 'reactstrap'
import styles from '../../styles/Navbar.module.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars } from '@fortawesome/free-solid-svg-icons'
import { faCommentDots } from '@fortawesome/free-solid-svg-icons'

function Navbar() {

  const openLeftSidebar = () => {
    document.getElementById('left_sidebar_id').style.transition = "all 0.5s ease-in-out";
    document.getElementById('left_sidebar_id').style.left = "0%";
  }

  const openRightSidebar = () => {
    document.getElementById('right_sidebar_id').style.display = "block";
  }

  return (
      <>
        <div className={styles.navbar_parent_div}>
          <div className={styles.navbar_inner_div}>
            <input type="text" className={styles.search_input_class} placeholder="Search"/>
            <img src='assets/images/search.png' className={styles.search_icon_class}/>
          </div>
        <FontAwesomeIcon icon={faBars} className={styles.open_sidebar_btn_class} onClick={openLeftSidebar}/>
        <FontAwesomeIcon icon={faCommentDots} className={styles.open_right_sidebar_btn_class} onClick={openRightSidebar}/>
        </div>
      </>
  )
}

export default Navbar