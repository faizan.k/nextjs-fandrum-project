import React, { useEffect } from 'react'
import { Button } from 'reactstrap';
import styles from '../../styles/LeftSidebar.module.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faXmark } from '@fortawesome/free-solid-svg-icons';


function LeftSidebar() {

    useEffect(() => {
        document.getElementById('discover_dropdown_main_div_id').style.display = "block";
        document.getElementById('discover_dropdown_main_div_id').style.height = "150px";
    },[])

    const closeLeftSidebar = () => {
        document.getElementById('left_sidebar_id').style.transition = "all 0.5s ease-in-out";
        document.getElementById('left_sidebar_id').style.left = "-80%";
    }

    const openDiscoverDropdown = () => {
        var discover = document.getElementById("discover_dropdown_main_div_id");
        if (discover.style.height == "150px") {
            document.getElementById('discover_dropdown_main_div_id').style.height = "0%";
            document.getElementById('discover_dropdown_main_div_id').style.display = "none";
        } else {
            document.getElementById('discover_dropdown_main_div_id').style.display = "block";
            document.getElementById('discover_dropdown_main_div_id').style.height = "150px";
        }
    }

    const openRewardsDropdown = () => {
        var rewards = document.getElementById("reawrds_dropdown_main_div_id");
        if (rewards.style.height == "150px") {
            document.getElementById('reawrds_dropdown_main_div_id').style.height = "0%";
            document.getElementById('reawrds_dropdown_main_div_id').style.display = "none";
        } else {
            document.getElementById('reawrds_dropdown_main_div_id').style.display = "block";
            document.getElementById('reawrds_dropdown_main_div_id').style.height = "150px";
        }
    }

    const openCompanyDropdown = () => {
        var company = document.getElementById("company_dropdown_main_div_id");
        var policies = document.getElementById("policies_dropdown_main_div_id");
        var help = document.getElementById("help_dropdown_main_div_id");
        if (help.style.height == "150px") {
            openHelpDropdown();
        }
        if (policies.style.height == "150px") {
            openPoliciesDropdown();
        }
        if (company.style.height == "150px") {
            document.getElementById('company_dropdown_main_div_id').style.height = "0%";
            document.getElementById('company_dropdown_main_div_id').style.display = "none";
        } else {
            document.getElementById('company_dropdown_main_div_id').style.display = "block";
            document.getElementById('company_dropdown_main_div_id').style.height = "150px";
        }
    }

    const openPoliciesDropdown = () => {
        var company = document.getElementById("company_dropdown_main_div_id");
        var policies = document.getElementById("policies_dropdown_main_div_id");
        var help = document.getElementById("help_dropdown_main_div_id");
        if (help.style.height == "150px") {
            openHelpDropdown();
        }
        if (company.style.height == "150px") {
            openCompanyDropdown();
        }
        if (policies.style.height == "150px") {
            document.getElementById('policies_dropdown_main_div_id').style.height = "0%";
            document.getElementById('policies_dropdown_main_div_id').style.display = "none";
        } else {
            document.getElementById('policies_dropdown_main_div_id').style.display = "block";
            document.getElementById('policies_dropdown_main_div_id').style.height = "150px";
        }
    }

    const openHelpDropdown = () => {
        var company = document.getElementById("company_dropdown_main_div_id");
        var policies = document.getElementById("policies_dropdown_main_div_id");
        var help = document.getElementById("help_dropdown_main_div_id");
        if (policies.style.height == "150px") {
            openPoliciesDropdown();
        }
        if (company.style.height == "150px") {
            openCompanyDropdown();
        }
        if (help.style.height == "150px") {
            document.getElementById('help_dropdown_main_div_id').style.height = "0%";
            document.getElementById('help_dropdown_main_div_id').style.display = "none";
        } else {
            document.getElementById('help_dropdown_main_div_id').style.display = "block";
            document.getElementById('help_dropdown_main_div_id').style.height = "150px";
        }
    }

    return (
        <>
            <div className={styles.sidebarParent} id="left_sidebar_id">
                <div className={styles.logo_parent_div}>
                    <img src='assets/images/logo.png' className={styles.logo_class} />
                </div>
                <div className={styles.links_parent_div}>
                    <div className={styles.links_div}>
                        <div className={styles.links_inner_class}>
                            <a href='#' style={{ textDecoration: "none" }}>
                                <img src='assets/images/home_icon.png' className={styles.links_icon_class} />
                                <span className={styles.links_text_class} style={{color:"red"}}>
                                    Home
                                </span>
                            </a>
                        </div>
                    </div>
                    <div className={styles.links_div}>
                        <div className={styles.dropdown_inner_class} onClick={openDiscoverDropdown}>
                            <div className="d-flex">
                                <img src='assets/images/discover_icon.png' className={styles.links_icon_class} />
                                <span className={styles.links_text_class}>
                                    Discover +
                                </span>
                            </div>
                            <div className={styles.dropdown_main_parent} id="discover_dropdown_main_div_id">
                                <div className={styles.links_inner_class} style={{ marginTop: "3px", marginLeft: "20px" }}>
                                    <a href='#' style={{ textDecoration: "none" }}>
                                        <span className={styles.links_text_class}>
                                            Movies
                                        </span>
                                    </a>
                                </div>
                                <div className={styles.links_inner_class} style={{ marginTop: "5px", marginLeft: "20px" }}>
                                    <a href='#' style={{ textDecoration: "none" }}>
                                        <span className={styles.links_text_class}>
                                            Shows
                                        </span>
                                    </a>
                                </div>
                                <div className={styles.links_inner_class} style={{ marginTop: "5px", marginLeft: "20px" }}>
                                    <a href='#' style={{ textDecoration: "none" }}>
                                        <span className={styles.links_text_class}>
                                            Watchlist
                                        </span>
                                    </a>
                                </div>
                                <div className={styles.links_inner_class} style={{ marginTop: "5px", marginLeft: "20px" }}>
                                    <a href='#' style={{ textDecoration: "none" }}>
                                        <span className={styles.links_text_class}>
                                            Subscription
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={styles.links_div}>
                        <div className={styles.dropdown_inner_class} onClick={openRewardsDropdown}>
                            <div className="d-flex">
                                <img src='assets/images/Group 261.svg' className={styles.links_icon_class} />
                                <span className={styles.links_text_class}>
                                    Rewards +
                                </span>
                            </div>
                            <div className={styles.dropdown_main_parent} id="reawrds_dropdown_main_div_id">
                                <div className={styles.links_inner_class} style={{ marginTop: "3px", marginLeft: "20px" }}>
                                    <a href='#' style={{ textDecoration: "none" }}>
                                        <span className={styles.links_text_class}>
                                            Link1
                                        </span>
                                    </a>
                                </div>
                                <div className={styles.links_inner_class} style={{ marginTop: "5px", marginLeft: "20px" }}>
                                    <span className={styles.links_text_class}>
                                        Link2
                                    </span>
                                </div>
                                <div className={styles.links_inner_class} style={{ marginTop: "5px", marginLeft: "20px" }}>
                                    <span className={styles.links_text_class}>
                                        Link3
                                    </span>
                                </div>
                                <div className={styles.links_inner_class} style={{ marginTop: "5px", marginLeft: "20px" }}>
                                    <span className={styles.links_text_class}>
                                        Link4
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={styles.links_div}>
                        <div className={styles.links_inner_class}>
                            <img src='assets/images/Group 265.svg' className={styles.links_icon_class} />
                            <span className={styles.links_text_class}>
                                Settings
                            </span>
                        </div>
                    </div>
                    <div className={styles.links_div}>
                        <div className={styles.links_inner_class}>
                            <img src='assets/images/discover_icon.png' className={styles.links_icon_class} />
                            <span className={styles.links_text_class}>
                                Promotions
                            </span>
                        </div>
                    </div>
                    <div className={styles.links_div}>
                        <div className={styles.links_inner_class}>
                            <img src='assets/images/power_settings_new_black_24dp.svg' className={styles.links_icon_class} />
                            <span className={styles.links_text_class}>
                                Logout
                            </span>
                        </div>
                    </div>
                </div>
                <div className={styles.sidebar_bottom_div_parent}>
                    <div className={styles.sidebar_bottom_inner_div}>
                        <div className={styles.bottom_links_div}>
                            <div className={styles.bottom_dropdown_inner_class} onClick={openCompanyDropdown}>
                                <div className="d-flex">
                                    <span className={styles.links_text_class}>
                                        Company +
                                    </span>
                                </div>
                                <div className={styles.dropdown_main_parent} id="company_dropdown_main_div_id">
                                    <div className={styles.links_inner_class} style={{ marginTop: "3px", marginLeft: "20px" }}>
                                        <a href='#' style={{ textDecoration: "none" }}>
                                            <span className={styles.links_text_class}>
                                                Movies
                                            </span>
                                        </a>
                                    </div>
                                    <div className={styles.links_inner_class} style={{ marginTop: "5px", marginLeft: "20px" }}>
                                        <a href='#' style={{ textDecoration: "none" }}>
                                            <span className={styles.links_text_class}>
                                                Shows
                                            </span>
                                        </a>
                                    </div>
                                    <div className={styles.links_inner_class} style={{ marginTop: "5px", marginLeft: "20px" }}>
                                        <a href='#' style={{ textDecoration: "none" }}>
                                            <span className={styles.links_text_class}>
                                                Watchlist
                                            </span>
                                        </a>
                                    </div>
                                    <div className={styles.links_inner_class} style={{ marginTop: "5px", marginLeft: "20px" }}>
                                        <a href='#' style={{ textDecoration: "none" }}>
                                            <span className={styles.links_text_class}>
                                                Subscription
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className={styles.bottom_links_div}>
                            <div className={styles.bottom_dropdown_inner_class} onClick={openPoliciesDropdown}>
                                <div className="d-flex">
                                    <span className={styles.links_text_class}>
                                        Policies +
                                    </span>
                                </div>
                                <div className={styles.dropdown_main_parent} id="policies_dropdown_main_div_id">
                                    <div className={styles.links_inner_class} style={{ marginTop: "3px", marginLeft: "20px" }}>
                                        <a href='#' style={{ textDecoration: "none" }}>
                                            <span className={styles.links_text_class}>
                                                Movies
                                            </span>
                                        </a>
                                    </div>
                                    <div className={styles.links_inner_class} style={{ marginTop: "5px", marginLeft: "20px" }}>
                                        <a href='#' style={{ textDecoration: "none" }}>
                                            <span className={styles.links_text_class}>
                                                Shows
                                            </span>
                                        </a>
                                    </div>
                                    <div className={styles.links_inner_class} style={{ marginTop: "5px", marginLeft: "20px" }}>
                                        <a href='#' style={{ textDecoration: "none" }}>
                                            <span className={styles.links_text_class}>
                                                Watchlist
                                            </span>
                                        </a>
                                    </div>
                                    <div className={styles.links_inner_class} style={{ marginTop: "5px", marginLeft: "20px" }}>
                                        <a href='#' style={{ textDecoration: "none" }}>
                                            <span className={styles.links_text_class}>
                                                Subscription
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className={styles.bottom_links_div} style={{ marginBottom: "20px" }}>
                            <div className={styles.bottom_dropdown_inner_class} onClick={openHelpDropdown}>
                                <div className="d-flex">
                                    <span className={styles.links_text_class}>
                                        Help +
                                    </span>
                                </div>
                                <div className={styles.dropdown_main_parent} id="help_dropdown_main_div_id">
                                    <div className={styles.links_inner_class} style={{ marginTop: "3px", marginLeft: "20px" }}>
                                        <a href='#' style={{ textDecoration: "none" }}>
                                            <span className={styles.links_text_class}>
                                                Movies
                                            </span>
                                        </a>
                                    </div>
                                    <div className={styles.links_inner_class} style={{ marginTop: "5px", marginLeft: "20px" }}>
                                        <a href='#' style={{ textDecoration: "none" }}>
                                            <span className={styles.links_text_class}>
                                                Shows
                                            </span>
                                        </a>
                                    </div>
                                    <div className={styles.links_inner_class} style={{ marginTop: "5px", marginLeft: "20px" }}>
                                        <a href='#' style={{ textDecoration: "none" }}>
                                            <span className={styles.links_text_class}>
                                                Watchlist
                                            </span>
                                        </a>
                                    </div>
                                    <div className={styles.links_inner_class} style={{ marginTop: "5px", marginLeft: "20px" }}>
                                        <a href='#' style={{ textDecoration: "none" }}>
                                            <span className={styles.links_text_class}>
                                                Subscription
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <p style={{ fontSize: "12px" }}>© 2021 Fandrum | All Rights Reserved</p>
                </div>
                <FontAwesomeIcon icon={faXmark} className={styles.close_sidebar_btn_class} onClick={closeLeftSidebar} />
            </div>
        </>
    );
}

export default LeftSidebar;