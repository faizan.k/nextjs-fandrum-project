import React , {useEffect, useState} from 'react'
import styles from '../../styles/RightSidebar.module.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faXmark } from '@fortawesome/free-solid-svg-icons';

function RightSidebar() {

  const [userStatus,setUserStatus] = useState("");
  
  useEffect(() => {
    setUserStatus("online");
  },[])

  const closeRigthSidebar = () => {
    document.getElementById('right_sidebar_id').style.transition = "all 0.5s ease-in-out";
    document.getElementById('right_sidebar_id').style.display = "none";
  }

  const toggleDropdownOptions = () => {
    var x = document.getElementById("head_user_options_div_id");
    if(x.style.display == "none"){
      document.getElementById('head_user_options_div_id').style.display = "block";
    }else{
      document.getElementById('head_user_options_div_id').style.display = "none";
    }
  }

  const handleUserOnlineStatus = () => {
    setUserStatus("online");
    setTimeout(() => {
      console.log(userStatus)
    },100)
  }

  const handleUserAwayStatus = () => {
    setUserStatus("away");  
    setTimeout(() => {
      console.log(userStatus)
    },100)
  }

  const online = {
    backgroundColor : "#2FDE69"
  }

  const away = {
    backgroundColor : "#FFB038"
  }

  return (
    <>
        <div className={styles.rightSidebarParent} id="right_sidebar_id">
            <div className={styles.right_sidebar_close_btns} onClick={closeRigthSidebar}>
              <FontAwesomeIcon icon={faXmark} className={styles.close_sidebar_btn_class}/>
            </div>
            <div className={styles.right_sidebar_header_parent}>
              <div className={styles.header_left_section}>
                <span style={{fontSize:"14px", color:"#610085", marginTop:"3px"}}>Hi,</span>
                <span style={{fontSize:"18px", color:"#610085"}}>Alisha Sen</span>
                <img src='assets/images/notification_sidebar_icon.png' className='mx-2' width="30px" height="30px"/>
              </div>
              <div className={styles.header_right_section} onClick={toggleDropdownOptions}>
                <div className={styles.online_icon_div} style = {userStatus === "online" ? online : away}></div>
                <span className='mx-1' style={{fontSize:"14px", marginTop:"5px"}}>
                  {
                    userStatus == "online" ? "Online" : "Away"
                  }
                </span>
                <img src='assets/images/user_icon_sidebar.png' width="35px" />
                <div className={styles.header_user_status_dropdown_parent} id="head_user_options_div_id">
                  <div className={styles.user_status_online_div}>
                    <div className={styles.online_icon_option_div}></div>
                    <p className={styles.online_icon_option_text} onClick={handleUserOnlineStatus}>Online</p>
                  </div>
                  <div className={styles.user_status_offline_div}>
                    <div className={styles.away_icon_option_div}></div>
                    <p className={styles.away_icon_option_text} onClick={handleUserAwayStatus}>Away</p>
                  </div>
                </div>
              </div>
            </div>
            <div className={styles.right_sidebar_second_div}>
              <div className={styles.second_div_left_parent}>
                <img src='assets/images/message_icon.png' width="25px"/>
                <span className='mx-2' style={{color:"#FF005C"}}>Messgaes</span>
              </div>
              <div className={styles.second_div_right_parent}>
                <img src='assets/images/sidebar_search_icon.png' width="15px" className='mx-3'/>
                <img src='assets/images/down_arrow_icon.png' width="15px"/>
              </div>
            </div>
            <div className={styles.right_sidebar_third_div}>
              <span className='mx-3' style={{fontSize:"#3D3D3D"}}>Today</span>
              <div className={styles.third_div_msg_parent}>
                <div className={styles.third_div_left_parent}>
                  <img src='assets/images/sidebar_msg_user_circle.png' className={styles.user_circle_icon_class}/>
                  <img src='assets/images/sidebar_user_msg_icon.png' className={styles.user_icon_class}/>
                  <div className={styles.msg_user_name_div}>
                    <span style={{fontSize:"14px",color:"#0099FF"}}>Emma Stone</span>
                    <br/>
                    <span style={{fontSize:"12px",color:"#7A8FA6"}}>Hey Hi!</span>
                  </div>
                </div>
                <div className={styles.msg_right_div}>
                  <span style={{fontSize:"10px"}}>8h ago</span>
                  <img src='assets/images/msg_close_icon.png' className='mx-2' width="15px" height="15px"/>
                </div>
              </div>
              <div className={styles.third_div_msg_parent}>
                <div className={styles.third_div_left_parent}>
                  <img src='assets/images/sidebar_msg_user_circle.png' className={styles.user_circle_icon_class}/>
                  <img src='assets/images/sidebar_user_msg_icon.png' className={styles.user_icon_class}/>
                  <div className={styles.msg_user_name_div}>
                    <span style={{fontSize:"14px",color:"#0099FF"}}>Emma Stone</span>
                    <br/>
                    <span style={{fontSize:"12px",color:"#7A8FA6"}}>Hey Hi!</span>
                  </div>
                </div>
                <div className={styles.msg_right_div}>
                  <span style={{fontSize:"10px"}}>8h ago</span>
                  <img src='assets/images/msg_close_icon.png' className='mx-2' width="15px" height="15px"/>
                </div>
              </div>
            </div>
            <div className={styles.right_sidebar_third_div}>
              <span className='mx-3' style={{fontSize:"#3D3D3D"}}>Yesterday</span>
              <div className={styles.third_div_msg_parent}>
                <div className={styles.third_div_left_parent}>
                  <img src='assets/images/sidebar_msg_user_circle.png' className={styles.user_circle_icon_class}/>
                  <img src='assets/images/sidebar_user_msg_icon.png' className={styles.user_icon_class}/>
                  <div className={styles.msg_user_name_div}>
                    <span style={{fontSize:"14px",color:"#0099FF"}}>Emma Stone</span>
                    <br/>
                    <span style={{fontSize:"12px",color:"#7A8FA6"}}>Hey Hi!</span>
                  </div>
                </div>
                <div className={styles.msg_right_div}>
                  <span style={{fontSize:"10px"}}>8h ago</span>
                  <img src='assets/images/msg_close_icon.png' className='mx-2' width="15px" height="15px"/>
                </div>
              </div>
              <div className={styles.third_div_msg_parent}>
                <div className={styles.third_div_left_parent}>
                  <img src='assets/images/sidebar_msg_user_circle.png' className={styles.user_circle_icon_class}/>
                  <img src='assets/images/sidebar_user_msg_icon.png' className={styles.user_icon_class}/>
                  <div className={styles.msg_user_name_div}>
                    <span style={{fontSize:"14px",color:"#0099FF"}}>Emma Stone</span>
                    <br/>
                    <span style={{fontSize:"12px",color:"#7A8FA6"}}>Hey Hi!</span>
                  </div>
                </div>
                <div className={styles.msg_right_div}>
                  <span style={{fontSize:"10px"}}>8h ago</span>
                  <img src='assets/images/msg_close_icon.png' className='mx-2' width="15px" height="15px"/>
                </div>
              </div>
            </div>
        </div>
    </>
  );
}

export default RightSidebar;